const workflow = require('@jetbrains/youtrack-scripting-api/workflow');
const config = require('./config');

function requestCreateBranchInWork(stateItem, ctx, connection) {
  const payload = {
    target_branch: ctx.issue.id,
    develop_branch: ctx.issue.fields[stateItem.developBranch]
  };
  
  const result = connection.postSync(stateItem.action, {}, payload);
  
  if (result) {
     const data = JSON.parse(result.response);
     if (data.status === 201 || data.status === 500) {
       workflow.message(data.message);
     }
  }
}

function requestCreateStageMr(stateItem,ctx, connection) {
   const payload = {title: ctx.issue.summary, source_branch: ctx.issue.id, url: ctx.issue.url, target_branch: ctx.issue.fields[stateItem.stageBranch], develop_branch: ctx.issue.fields[stateItem.developBranch], recipients_email:config.RECIPIENTS_EMAIL};
   const result = connection.postSync(stateItem.action, {}, payload);

   if (result) {
     const data = JSON.parse(result.response);
     if (data.status === 200 || data.status === 500) {
       workflow.message(data.message);
     } else if (data.status === 201) {
       workflow.message(data.message);
       ctx.issue.addComment(`youtrack создал автоматически MR - ${data.webUrl}`);
     }
   }
}

function requestCreateDevelopMr(stateItem,ctx, connection) {
   const payload = {title: ctx.issue.summary, source_branch: ctx.issue.id, url: ctx.issue.url, target_branch: ctx.issue.fields[stateItem.developBranch], master_branch: ctx.issue.fields[stateItem.masterBranch], recipients_email:config.RECIPIENTS_EMAIL};

  const result = connection.postSync(stateItem.action, {}, payload);

  if (result) {
     const data = JSON.parse(result.response);
     if (data.status === 200 || data.status === 500) {
       workflow.message(data.message);
     } else if (data.status === 201) {
       workflow.message(data.message);
       ctx.issue.addComment(data.message);
     }
   }
}

function requestDeleteReleaseMr(ctx, connection) {
  const payload = {
    version: ctx.issue.fields[config.CONFIG.versionField],
    source_branch: config.RELEASE_BRANCH_NAME,
    target_branch: ctx.issue.fields[config.BRANCH_FIELDS.masterBranch],
    master_branch: ctx.issue.fields[config.BRANCH_FIELDS.masterBranch],
  };

  const result = connection.postSync(config.ACTIONS.deleteReleaseMr, {}, payload);

  return result;
}

function requestCreateReleaseMr(ctx, connection) {
  const tasks = [];
  let deleteTasks = false;
  let createMr = false;

  ctx.issue.links['parent for'].forEach(function (dep) {
    const payload = {title: dep.summary, source_branch: dep.id, url: dep.url};
    if (dep.fields.State.name !== config.STATE.release) {
      workflow.message(`${payload.source_branch} не находится в состоянии ${config.STATE.release}. Ветка не будет добавлена в релиз!`);
    } else {
      tasks.push(payload);
    }
  });

  const payload = {
    version: ctx.issue.fields[config.CONFIG.versionField],
    source_branch: config.RELEASE_BRANCH_NAME,
    target_branch: ctx.issue.fields[config.BRANCH_FIELDS.masterBranch],
    master_branch: ctx.issue.fields[config.BRANCH_FIELDS.masterBranch],
    tasks: tasks,
    recipients_email: config.RECIPIENTS_EMAIL
  };
  const payloadUpdateTasks = {
     version: ctx.issue.fields[config.CONFIG.versionField],
     target_branch: config.RELEASE_BRANCH_NAME,
     tasks: tasks
  };

//   const responseDelete = requestDeleteReleaseMr(ctx, connection);
  let responseCreateReleaseMr;
  let responseUpdateIssue;
  deleteTasks = true;
//   if (responseDelete) {
//      const data = JSON.parse(responseDelete.response);
//     if (data.status === 200) {
//       deleteTasks = true;
//        workflow.message(data.message);
//     }
//     else {
//       workflow.message(data.message);
//     }
//   }
  
  if (deleteTasks) {
    responseCreateReleaseMr = connection.postSync(config.ACTIONS.createReleaseMr, {}, payload);
  }
  
  if (responseCreateReleaseMr) {
    const data = JSON.parse(responseCreateReleaseMr.response);
    if (data.status === 201) {
       createMr = true;
       workflow.message(data.message);
       ctx.issue.addComment(`youtrack создал автоматически Release MR - ${data?.url}`);
    } 
     else {
       workflow.message(data.message);
     }
  }
  
  if (createMr) {
  	responseUpdateIssue = connection.postSync(config.ACTIONS.updateReleaseIssue, {}, payloadUpdateTasks);    
  }
  

  if (responseUpdateIssue) {
     const data = JSON.parse(responseUpdateIssue.response);
     if (data.status === 201) {
       	data.messages.forEach(item => {
         workflow.message(item);
       	});
     } else if (data.status === 500) {
       data.messages.forEach(item => {
         workflow.message(item);
       });
     }
  }
}

exports.requestCreateStageMr = requestCreateStageMr;
exports.requestCreateDevelopMr = requestCreateDevelopMr;
exports.requestCreateReleaseMr = requestCreateReleaseMr;
exports.requestCreateBranchInWork = requestCreateBranchInWork;