import { readFile } from 'fs/promises';
import { zip } from 'zip-a-folder';

class Compressor {

  static async main(version) {
    await zip('./youtrack-workflow', `./zip-worflow/youtrack-worflow-${version}.zip`);
  }
}

try {
  const data = await readFile('package.json', 'utf8');
  const packageJson = JSON.parse(data);

  const version = packageJson.version;

  console.log('Версия из package.json:', version);
  await Compressor.main(version)
} catch (err) {
  console.error(err);
}