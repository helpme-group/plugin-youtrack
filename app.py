from flask import Flask
from youtrack_gitlab_api.routes.main import routes
from youtrack_gitlab_api.environ import PORT, DEBUG

app = Flask('webhook')

app.register_blueprint(routes, url_prefix='/')
if __name__ == '__main__':
    app.run(debug=DEBUG, port=PORT)
