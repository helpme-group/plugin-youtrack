from youtrack_gitlab_api.utils.helpers import get_opened_merge_requests, wait_mr_pass_checking, check_branches_exists_regex
from youtrack_gitlab_api.constants import Status
import logging


def create_develop_mr(project, kwargs):
    try:
        existed_branches = check_branches_exists_regex(project, f'^{kwargs["source_branch"]}$')
        if len(existed_branches) == 0:
            return {"status": Status.ERROR,
                    'message': f'{kwargs["source_branch"]} данной ветки не существует в репозитории!'}

        all_opened_mrs = get_opened_merge_requests(project)
        existing_mr = None

        for opened_mr in all_opened_mrs:
            if (opened_mr.source_branch == kwargs['source_branch']) and (
                    opened_mr.target_branch == kwargs['target_branch']):
                existing_mr = opened_mr
                break

        if existing_mr is None:
            new_mr = project.mergerequests.create(
                {'source_branch': kwargs['source_branch'], 'target_branch': kwargs['target_branch'],
                 'title': f"{kwargs['title']}", 'description': kwargs['description']})
            wait_mr_pass_checking(project, new_mr)

            existing_mr = new_mr

        merge_request = project.mergerequests.get(existing_mr.get_id())
        changes_mr = merge_request.changes()

        if changes_mr['changes_count'] is None:
            merge_request.state_event = 'close'
            merge_request.save()

            return {"status": Status.ERROR,
                    'message': f'Не удалось слить mr в {kwargs["target_branch"]} не было изменений в исходной ветке!'}

        if merge_request.has_conflicts:
            return {"status": Status.ERROR, 'message': f'Не удалось слить mr в {kwargs["target_branch"]} возникли '
                                                       f'конфликты!'}

        merge_request.merge()
        return {"status": Status.SUCCESS, 'mr': merge_request}
    except Exception as e:
        logging.error(e)
        return {"status": Status.ERROR, 'message': f'Не удалось слить mr в {kwargs["target_branch"]}!'}
