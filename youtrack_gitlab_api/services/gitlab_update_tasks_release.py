import logging
from youtrack_gitlab_api.constants import Status
from youtrack_gitlab_api.services.gitlab_develop_mr import create_develop_mr


def update_tasks_release(project, kwargs):
    try:
        messages = []
        release_branch_info = {
            'tasks': kwargs['tasks'],
            'target_branch': kwargs['target_branch'],
        }
        for task in release_branch_info['tasks']:
            task_info = {
                'title': f"{task['source_branch']}: {task['title']}",
                'description': task['url'],
                'source_branch': task['source_branch'],
                'target_branch': release_branch_info['target_branch'],
            }

            create_mr = create_develop_mr(project, task_info)

            if create_mr['status'] == Status.SUCCESS:
                messages.append(f'MR {task_info["source_branch"]} смерджен в {task_info["target_branch"]}')
            elif create_mr['status'] == Status.ERROR:
                messages.append(create_mr['message'])
            return {"status": Status.SUCCESS, 'messages': messages}
    except Exception as e:
        logging.error(e)
        return {"status": Status.ERROR, 'messages': [f'Не удалось обновить задачи в {kwargs["target_branch"]}']}
