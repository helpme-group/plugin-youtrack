from youtrack_gitlab_api.environ import SMTP_SERVER, SMTP_PASSWORD, SMTP_HOST, SMTP_PORT
import smtplib
from email.mime.text import MIMEText
from email.header import Header


def send_email(recipients_email, subject, body):
    if len(recipients_email) == 0:
        return
    login = SMTP_SERVER
    password = SMTP_PASSWORD
    host = SMTP_HOST
    port = SMTP_PORT

    msg = MIMEText(f'{body}', 'plain', 'utf-8')
    msg['Subject'] = Header(f'{subject}', 'utf-8')
    msg['From'] = login
    msg['To'] = ', '.join(recipients_email)

    s = smtplib.SMTP(host, port, timeout=10)

    try:
        s.starttls()
        s.login(user=login, password=password)
        s.sendmail(msg['From'], recipients_email, msg.as_string())
    except Exception as e:
        print(e)
    finally:
        s.quit()
