import re
import time

from youtrack_gitlab_api.constants import MR_CHECK_INTERVAL, MR_CHECK_TIMEOUT


def wait_mr_pass_checking(project, mr):
    timeout = MR_CHECK_TIMEOUT
    interval = MR_CHECK_INTERVAL
    count = int(timeout / interval)

    while project.mergerequests.get(mr.get_id()).detailed_merge_status == 'checking':
        count -= 1

        if count == 0:
            raise Exception('Таймаут проверки MR')
        time.sleep(interval)


def check_branches_exists_regex(project, regex):
    project_branches_info = project.branches.list()

    project_branches_name = []
    for br in project_branches_info:
        project_branches_name.append(br.name)

    r = re.compile(regex)

    existed_branches = list(filter(r.match, project_branches_name))

    return existed_branches


def get_opened_merge_requests(project):
    all_opened_me = project.mergerequests.list(state='opened', get_all=True)
    return all_opened_me


def get_merged_merge_request(project):
    list_merged_mr = project.mergerequests.list(state='merged', get_all=True)

    return list_merged_mr


def get_all_branches_find(project, prefix):
    all_branches_find = project.branches.list(regex=f'^{prefix}-', get_all=True)
    return all_branches_find


def get_all_branches_by_project_id(project):
    all_branches_project = project.branches.list(get_all=True)
    return all_branches_project


def get_all_branches_by_commit_id(project, commit_id):
    all_commits_by_id = project.commits.get(commit_id)
    commit_branches = all_commits_by_id.refs('branch', get_all=True)

    return commit_branches


def get_branches_by_name(project, name_branch):
    project_branches_info = project.branches.list()

    project_branches_name = []
    for br in project_branches_info:
        if name_branch == br.name:
            project_branches_name.append(br.name)

    return project_branches_name
